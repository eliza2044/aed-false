# AED-false

This repository holds subset of data published at https://serv.cusp.nyu.edu/projects/urbansounddataset/ by New York University The original dataset contains 8,732 labelled sound clips (4 seconds each) from ten classes: air conditioner, car horn, children playing, dog bark, drilling, engine idling, gunshot, jackhammer, siren, and street music.

Ths repository has been created to service the needs of internal project Hackathons of the H2020 ASGARD project and its intended purpose is for research purposes only. The repository contains a mix of events from the aforementioned dataset.
## Datasets were created by

Justin Salamon*^, Christopher Jacoby* and Juan Pablo Bello*
* Music and Audio Research Laboratory (MARL), New York University
^ Center for Urban Science and Progress (CUSP), New York University



